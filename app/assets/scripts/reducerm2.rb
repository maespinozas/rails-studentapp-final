#!/usr/bin/env ruby
=begin
to test outside hadoop cat fakedata.json | ruby mapperv2.rb | sort -k1,1 |ruby reducerv2.rb
GPA CONV. TABLE
A+ 97-100  4.0
A 93-96    4.0
A-  90-92   3.7
B+  87-89   3.3
B   83-89   3.0
B-  80-82   2.7
C+  77-79   2.3
C   73-76   2.0
C-  70-72   1.7
D+  67-69   1.3
D   65-66   1.0

metric id metric
1       %track
2       Av GPA overall
3       Av GPA per term
4       Av GPA per Course
=end

metric_flag=true
nlines=0
$metricid=2
$n_st=0
$n_st_ontrack=0
$pontrack=0
$grades_dict = Hash.new(0)  #to have default value 0 and not nil
$grades_dict={"A+"=>4.0,"A"=>4.0 \
,"A-"=>3.7,"B+"=>3.3,"B"=>3.0, \
"B-"=>2.7,"C+"=>2.3 \
,"C"=>2.0,"C-"=>1.7 \
,"D"=>1.3,"D-"=>1.0}
$term_gpa = Hash.new(0)  #to have default value 0 and not nil
$term_gpa={"1"=>0.0,"n_st_1"=>0.0,"2"=>0.0,"n_st_2"=>0.0 \
,"3"=>0.0,"n_st_3"=>0.0,"4"=>0.0,"n_st_4"=>0.0,"5"=>0.0,"n_st_5"=>0.0 \
,"6"=>0.0,"n_st_6"=>0.0,"7"=>0.0,"n_st_7"=>0.0}  #n_st_7 number of students in term 7
$gpa=0.0
$n_students=0.0
$prev_course=""   #to be used in metric 4


def metric(mid,line)
  case mid
  when 2,3  #GETTING A,B,A,C-, etc
    stop=false
    n_courses=1.0
    gpast=0.0   #gpa single student
    n_students_in_term=0.0  #Used fot metric 3
    if(mid==3)
      term=line[line.index("T")+1...line.index(',')]
      line=line[line.index(',')+1...line.size()]    #shift line
      tmp_string="n_st_"+term.to_s
    end
    while(stop==false)    #do this for each line
      if(line.index(',')==nil)
        grade=line
      else
        grade=line[0...line.index(',')]
        line=line[line.index(',')+1...line.size()]
      end
      gpast+=$grades_dict[grade].to_f
      n_courses+=1
      #$n_students+=1    #used for global GPA
      #print(grade)
      if(line.index(',')==nil)
        grade=line        #get last grade
        gpast+=$grades_dict[grade].to_f
        gpast=gpast/n_courses.to_f    #GPA for a single student
        #puts("GPA:"+gpast.to_s)
        $gpa+=gpast                   #accumulative gpa for the cohort
        if(mid==3)
          $term_gpa[tmp_string]+=1  #add one student to the term
          $term_gpa[term.to_s]+=gpast    #accumulate gpas for all students in that term
        end
        stop=true
        break
      end
    end
  else
    puts("hello cindy")
  end
end

STDIN.each_line do |line|
  #puts($metricid)
  line=line.strip()
  metric($metricid,line)
  $n_st+=1.0           #number of lines=number of students
end
### After processing all the lines
puts("Av GPA for the cohort:,"+($gpa/$n_st).round(4).to_s)
