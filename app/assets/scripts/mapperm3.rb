#!/usr/bin/env ruby
=begin
STUDENT ANALYTICS MAPPER

metric id metric
1       %track
2       Av GPA overall
3       Av GPA per term
4       Av GPA per Course
5       Students and GPA
6       Course GPA and number of students
=end

$metricid=3
$n_st=0
$n_st_ontrack=0
$pontrack=0

def metric(mid,line)
  case mid
  when 2,3  ## for each student the list of grades, ex. A,A-,B,C,C+
    stop=""
    grade=""
    termindex=line.index("semesters_attended:")+"semesters_attended:".size+1  #ex. semesters_attended: 6
    term=line[termindex]
    while(stop!=nil)
        #get the grade: position
        break if(line[2]=="]") #ex. grade:"B-"}],track:"False",ptrack:0.3
        start=line.index("grade:")+6+1    #grade: 6
        #puts(start)
        if(start==nil)
          stop=start
          break
        end
        endindex=line[start...line.size()].index('"') #get the end of grade ex. C"
        #puts(endindex)
        if(grade.size()==0)
          grade+=line[start...start+endindex]
        else
          grade+=","+line[start...start+endindex]
        end
        line=line[start+endindex...line.size()]  #to get next grade start from where last grade ended
    end
    if mid==3
      print("T"+term.to_s+","+grade+"\n")     #ex. T3,A,A-,B,C,C+
    else
      puts(grade)                         #ex. A,A-,B,C,C+
    end
  else
    puts("hello cindy")
    ##something
  end
end

## MAIN ###
metric_flag=true
STDIN.each_line do |line|
  metric($metricid,line)
end
