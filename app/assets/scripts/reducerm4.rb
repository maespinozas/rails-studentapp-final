#!/usr/bin/env ruby
=begin
### REDUCER ####
to test outside hadoop cat fakedata.json | ruby mapperv2.rb | sort -k1,1 |ruby reducerv2.rb
GPA CONV. TABLE
A+ 97-100  4.0
A 93-96    4.0
A-  90-92   3.7
B+  87-89   3.3
B   83-89   3.0
B-  80-82   2.7
C+  77-79   2.3
C   73-76   2.0
C-  70-72   1.7
D+  67-69   1.3
D   65-66   1.0

metric id metric
1       %track
2       Av GPA overall
3       Av GPA per term
4       Av GPA per Course
5       Students and GPA
6       Course GPA and number of students
=end

metric_flag=true
nlines=0
$metricid=4
$n_st=0
$n_st_ontrack=0
$pontrack=0
$grades_dict = Hash.new(0)  #to have default value 0 and not nil
$grades_dict={"A+"=>4.0,"A"=>4.0 \
,"A-"=>3.7,"B+"=>3.3,"B"=>3.0, \
"B-"=>2.7,"C+"=>2.3 \
,"C"=>2.0,"C-"=>1.7 \
,"D"=>1.3,"D-"=>1.0}
$term_gpa = Hash.new(0)  #to have default value 0 and not nil
$term_gpa={"1"=>0.0,"n_st_1"=>0.0,"2"=>0.0,"n_st_2"=>0.0 \
,"3"=>0.0,"n_st_3"=>0.0,"4"=>0.0,"n_st_4"=>0.0,"5"=>0.0,"n_st_5"=>0.0 \
,"6"=>0.0,"n_st_6"=>0.0,"7"=>0.0,"n_st_7"=>0.0}  #n_st_7 number of students in term 7
$gpa=0.0
$n_students=0.0
$prev_course=""   #to be used in metric 4


def metric(mid,line)
  case mid
  when 4  #Av GPA per course
    course=line[0...line.index(',')]
    grade=line[line.index(',')+1...line.size()]
    #puts(grade)
    #puts($grades_dict[grade])
    if (course!=$prev_course)
      if($prev_course.size()>1)
        $gpa+=$grades_dict[grade].to_i#new course, print GPA information about this one
        $n_students+=1.0
        $gpa=$gpa/$n_students
        $prev_course=$prev_course.rstrip
        puts($prev_course.to_s+":,"+'%.2f'% $gpa.to_s)
        $gpa=0.0            #reset for next course
        $n_students=0.0
      end
      $prev_course=course         #new course
    else                          #same course again, add the GPA
      $n_students+=1.0
      $gpa+=$grades_dict[grade].to_f
    end
  when String
    puts("Fixnum")
  else
    puts("else case")
    ##something
  end
end

STDIN.each_line do |line|
  #puts($metricid)
  line=line.strip
  metric($metricid,line)
  $n_st+=1.0           #number of lines=number of students
end
