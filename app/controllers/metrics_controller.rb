class MetricsController < ApplicationController
  before_action :set_metric, only: [:show, :edit, :update, :destroy]


  def self.displaymetricCC
    #text=File.open('/home/mrhyde/out.txt').read
    file_path=Rails.root.join('app','assets','out.txt').to_s
    if File.exists?(file_path)
    text=File.open(Rails.root.join('app','assets','out.txt').to_s).read
      text.each_line do |line|
         puts(line)
      end
    else
      puts("nothing to show")
    end
  end

  def self.displaymetric
    #text=File.open('/home/mrhyde/out.txt').read
    file_path=Rails.root.join('app','assets','out.txt').to_s
    if File.exists?(file_path)
    @text=File.open(Rails.root.join('app','assets','out.txt').to_s).readlines.join("<br>").html_safe
      @text.gsub(",","").html_safe
    else
      #
    end

  end

  def self.getArrays
    #text=File.open('/home/mrhyde/out.txt').read
    file_path=Rails.root.join('app','assets','out.txt').to_s
    #data=[[],[],[],[]]
    data={}
    c=0
    if File.exists?(file_path)
    text=File.open(Rails.root.join('app','assets','out.txt').to_s).read
        text.each_line do |line|
          x=line.split(",")
          if x.size()>1
            data[c]=x
            c+=1
          end
        end
        data
        #example of this output for the TERM and GPA metric
        '''{0=>["TERM 1", " GPA:2.298571428571429\n"],
        1=>["TERM 2", " GPA:2.3673611111111104\n"],
        2=>["TERM 3", " GPA:2.3366867469879504\n"],
        3=>["TERM 4", " GPA:2.37171875\n"],
        4=>["TERM 5", " GPA:2.374964028776979\n"],
        5=>["TERM 6", " GPA:2.3698581560283696\n"],
        6=>["TERM 7", " GPA:2.3854487179487185\n"]}

        data[0][0] if we want to extract TERM1
        data[0][1] if we want to extract GPA.xxxx

        '''
    else
      #
    end
  #data
  end

    $term=[]
  $gpa=[]
  def self.getgpa
    file_path=Rails.root.join('app','assets','out.txt').to_s
    #data=[[],[],[],[]]
    gpa=[]
    c=0
    if File.exists?(file_path)
    text=File.open(Rails.root.join('app','assets','out.txt').to_s).read
        text.each_line do |line|
          x=line.split(",")
          #if(x.index("GPA")>0)
            gpa[c]=x[1][5...x[1].size-2].to_f;
          #else
          if(x[1].index("GPA")==nil)
            gpa[c]=x[1].to_f;
          end
            c+=1
          end
          $gpa=gpa
          return $gpa
    end
  end

  def self.getTerm
    file_path=Rails.root.join('app','assets','out.txt').to_s
    #data=[[],[],[],[]]
    term=[]
    c=0
    if File.exists?(file_path)
    text=File.open(Rails.root.join('app','assets','out.txt').to_s).read
        text.each_line do |line|
          x=line.split(",")
            term[c]=x[0][5].to_i;
            c+=1
          end
          $term=term
          return $term
    end
  end

  def self.getCourse
    file_path=Rails.root.join('app','assets','out.txt').to_s
    course=[]
    c=0
    if File.exists?(file_path)
    text=File.open(Rails.root.join('app','assets','out.txt').to_s).read
        text.each_line do |line|
          x=line.split(",")
            course[c]=x[0].to_s
            c+=1
          end
          return course
    end
  end


#REAL HADOOP FUNCTION
  #def self.hadoop
  def hadoopREAL
    script=Rails.root.join('app','assets','mapreduce.sh').to_s #for hadoop
    destfolder=Rails.root.join('app','assets').to_s #where the out.txt resulting from hadoop will be stored
    script='sh "'+script+'" '+Metric.last.metric_id.to_s+" "+destfolder
    system(script)	#to run hadoop
  end


#TESTING TO RUN SCRIPTS LOCALLY - NO HADOOP
def hadoopFake
  script=Rails.root.join('app','assets','mapreduce_local.sh').to_s #for hadoop
  scriptsfolder=Rails.root.join('app','assets','scripts').to_s #where the out.txt resulting from hadoop will be stored
  puts("########################")
  puts(scriptsfolder)
  destfolder=Rails.root.join('app','assets').to_s #where the out.txt resulting from hadoop will be stored
  puts("########################")
  puts('"'+destfolder+'"')
  script='sh "'+script+'" '+Metric.last.metric_id.to_s+' "'+destfolder+'" "'+scriptsfolder+'"'
  puts(script)
  system(script)	#to run hadoop
end


  # GET /welcome
  # create a view in touch app/views/metrics/welcome.html.erb
  # add the route in route rb as follows
  # get '/welcome', to:"metrics#welcome"
  def welcome
    #
  end
  # get '/choose', to:"metrics#choose"
  def choose
    #redirect_to(static_pages_metricresults_path)
  end
  # GET /metrics
  # GET /metrics.json
  def index
    @metrics = Metric.all
  end

  # GET /metrics/1
  # GET /metrics/1.json
  def show
  end

  # GET /metrics/new
  def new
    @metric = Metric.new
  end

  # GET /metrics/1/edit
  def edit
  end

  def upload
    #
    uploaded_io = params[:metric][:text]
    File.open(Rails.root.join('app', 'assets', uploaded_io.original_filename), 'wb') do |file|
      file.write(uploaded_io.read)
    end
  end

  # POST /metrics
  # POST /metrics.json
  def create
    @metric = Metric.new(metric_params)
    @dept=Dept.new(dept_params)
    @dept.save

    respond_to do |format|
      if @metric.save

	    ## Ha freaking doop #####
    	#script=Rails.root.join('app','assets','mapreduce.sh').to_s #for hadoop
    	#destfolder=Rails.root.join('app','assets').to_s #where the out.txt resulting from hadoop will be stored
    	#dept=Dept::DEPT_OPTIONS.key(Dept.last.dept_id)  #CS ECE etc
    	#script='sh "'+script+'" '+Metric.last.metric_id.to_s+" "+dept+" "+destfolder
    	#system(script)	#to run hadoop
    	######################
      ##Adding Hadoop FAKE:RUNNING SCRIPTS LOCALLY
      script=Rails.root.join('app','assets','mapreduce_local.sh').to_s #for hadoop
      dept=Dept::DEPT_OPTIONS.key(Dept.last.dept_id)  #CS ECE etc
      scriptsfolder=Rails.root.join('app','assets','scripts').to_s #where the out.txt resulting from hadoop will be stored
      puts("########################")
      puts(scriptsfolder)
      destfolder=Rails.root.join('app','assets').to_s #where the out.txt resulting from hadoop will be stored
      puts("########################")
      puts('"'+destfolder+'"')
      script='sh "'+script+'" '+Metric.last.metric_id.to_s+' '+dept+' "'+destfolder+'" "'+scriptsfolder+'"'
      #puts(script)
      system(script)	#to run hadoop


        #format.html { redirect_to @metric, notice: 'Metric was successfully created.' }
        format.html { redirect_to static_pages_metricresults_path, notice: 'Metric was successfully created.' }
        format.json { render :show, status: :created, location: @metric }
      else
        format.html { render :new }
       format.json { render json: @metric.errors, status: :unprocessable_entity }
      end
    end



  end

  # PATCH/PUT /metrics/1
  # PATCH/PUT /metrics/1.json
  def update
    respond_to do |format|
      if @metric.update(metric_params)
        format.html { redirect_to @metric, notice: 'Metric was successfully updated.' }
        format.json { render :show, status: :ok, location: @metric }
      else
        format.html { render :edit }
        format.json { render json: @metric.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /metrics/1
  # DELETE /metrics/1.json
  def destroy
    @metric.destroy
    respond_to do |format|
      format.html { redirect_to metrics_url, notice: 'Metric was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_metric
      @metric = Metric.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def metric_params
      params.require(:metric).permit(:metricname, :metric_id, :text)
    end
    def dept_params
      params.require(:dept).permit(:deptname, :dept_id)
    end

end

#def self.checkFileAndDisplay
#file_path=Rails.root.join('app','assets','out.txt').to_s
#  if File.exists?(file_path)
#    text=File.open(Rails.root.join('app','assets','out.txt').to_s).read
#    text.each_line do |line|
#      puts(line+"\n")
 #  end
 #   File.delete(file_path)
 #else
 #   require 'timers'
 #   timers = Timers::Group.new
 #   every_five_seconds = timers.every(5) { puts "Another 30 seconds" }
 #   loop { timers.wait }
 # end
#end

#DUMMY SCRIPT FUNCTION
#def bash
#  script=Rails.root.join('app','assets','hello.sh').to_s #for dummy script
#  destfolder=Rails.root.join('app','assets').to_s #where the out.txt resulting from hadoop will be stored
#  script='sh "'+script+'" '+Metric.last.metric_id.to_s
#  system(script)	#to run hadoop
#end
