json.array!(@metrics) do |metric|
  json.extract! metric, :id, :metricname, :metric_id, :deptname, :dept_id
  json.url metric_url(metric, format: :json)
end
